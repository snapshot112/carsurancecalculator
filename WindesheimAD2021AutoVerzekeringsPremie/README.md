# Introduction

Welkom bij de carsurance applicatie.
deze README gaat vooral over de **tests** en de **gevonden fouten** in de code.


## Tests

### PolicyHolderTests

#### LicenseAgeTest

##### Het doel van de UnitTest
Het doel van deze unittest was om het enigste stukje logica in het aanmaken van een policyholder te testen en te kijken of er de juiste datum uit kwam.

##### Welke data is er gebruikt
ik heb als datums meegegeven:
1. 1 jaar 0 maanden en 0 dagen in de toekomst
2. 1 jaar 0 maanden en 0 dagen geleden
3. 10 jaar 4 maanden en 20 dagen geleden
4. 0 jaar 11 maanden en 27 dagen geleden
5. 10 jaar -4 maanden en -20 dagen in de toekomst

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 5 testen of negatieve waardes goed worden opgevangen
1, 2, 4 grenswaardes van net wel en net geen jaar
3, 5 een positieve en negatieve normale waarde om te kijken of ook dat goed werkt


### VehicleTests

#### AgeTest

##### Het doel van de UnitTest
Het doel van deze unittest was om het enigste stukje logica in het aanmaken van een vehicle te testen en te kijken of er het juiste bouwjaar uit kwam.

##### Welke data is er gebruikt
1. 0 jaar geleden
2. 1 jaar geleden
3. 1 jaar in de toekomst
4. 10 jaar in de toekomst
5. 10 jaar geleden
6. 60 jaar geleden

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 2, 3 kijken hoe hij met 0 en lage waardes omgaat
3, 4 kijken hoe hij met negatieve getallen omgaat
4, 5 kijken hoe hij met normale waardes omgaat
6 kijken hoe hij met een hoge waarde omgaat


### PremiumCalculationTests

##### Welke data is er gebruikt
Eerst ga ik het hebben over de data die ik heb gebruikt als invulling voor de testen waar deze data niet nodig was.
En waarom ik voor deze data heb gekozen

###### de standaard PolicyHolders:
1. 28 jaar oud
2. postcode: 5555
3. Schadevrije jaren: 5
4. rijbewijs is 6 jaar 10 maanden en 20 dagen geleden gehaald

###### de standaard vehicles
1. power = 180 KW
2. prijs = 30000
3. leeftijd = 5 jaar

###### het standaard verzekeringstype
1. WA

##### Welke technieken zijn gebruikt om deze data te kiezen

###### de standaard PolicyHolders:
1. 28 jaar oud is meer dan de maximaal 22 jaar wat voor extra toeslag zou zorgen
2. een postcode van 5555 is meer dan de maximale 4499 wat voor extra toeslag zou zorgen
3. 5 schadevrije jaren is minder dan de 6 waarvoor je korting zou krijgen
4. 6 jaar is meer dan de 4 jaar waarmee je extra toeslag zou moeten betalen

1, 2, 3, 4 deze waardes vermijden zoveel mogelijk kortingen en toeslagen zodat er zo min mogelijk code coverage is

###### de standaard vehicles
1, 2, 3 dit zijn gewoon willekeurige waardes,
	je krijgt geen extra toeslag ofzo van specifieke waardes
	en het maakt dus niks uit wat je hier invult zolang het maar een normale waarde is

###### het standaard verzekeringstype
1. WA geeft geen extra toeslag en geeft daarom het minste code coverage


#### PremiumLicenseAgeSurchargeTest

##### Het doel van de UnitTest
Deze test test of de toeslag voor het minder dan 5 jaar je rijbewijs hebben goed wordt toegepast

##### Welke data is er gebruikt
1. 5 jaar 0 maanden en 0 dagen
2. 4 jaar 11 maanden en 27 dagen
3. 6 jaar 10 maanden en 20 dagen

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 2 randwaarde voor de 5 jaar
3 een willekeurige waarde boven de 5 jaar die ook geen toeslag zou moeten krijgen


#### PremiumAgeSurchargeTest

##### Het doel van de UnitTest
Deze test test of de toeslag voor het jonger dan 23 jaar oud zijn goed wordt toegepast

##### Welke data is er gebruikt
1. 22 jaar oud
2. 23 jaar oud
3. 24 jaar oud

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 2 randwaarde voor de 23 jaar
3 een waarde hoger dan de 23 jaar om te kijken of dat ook goed werkt


#### PremiumNoClaimYearsDiscountTest

##### Het doel van de UnitTest
Deze test test of de korting voor het langer dan 5 jaar schadevrij rijden goed wordt toegepast

##### Welke data is er gebruikt
1. 5 jaar
2. 6 jaar
3. 18 jaar
4. 19 jaar
5. 12 jaar

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 2 randwaarden voor de 5 jaar
3, 4 randwaarden voor de max 65% korting
5 een willekeurig getal om te kijken of dat ook goed wordt toegepast


#### PremiumPostalCodeSurchargeTest

##### Het doel van de UnitTest
Deze test test of de toeslag voor het hebben van bepaalde postcodes goed wordt toegepast

##### Welke data is er gebruikt
1. 0999
2. 1000
3. 3599
4. 3600
5. 4499
6. 4500
7. 5555

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 2, 3, 4 randwaarden voor de postcodes van 10xx - 35xx
3, 4, 5, 6 randwaarden voor de postcodes van 36xx - 44xx
7 waarde die buiten de toeslagen vallen


#### PremiumInsuranceTypeSurchargeTest

##### Het doel van de UnitTest
Deze test test of de toeslag voor het kiezen van een bepaald type verzekering goed wordt toegepast

##### Welke data is er gebruikt
1. WA
2. WA_Plus
3. AllRisk

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 2, 3 alle 3 de mogelijke opties worden 1 maal getest


#### PremiumMonthlyPaymentSurchargeTest

##### Het doel van de UnitTest
Deze test test of de juiste prijzen voor de juiste periodes worden weergeven wanneer je een periode selecteerd

##### Welke data is er gebruikt
1. Month
2. Year

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 2 alle 2 de mogelijke opties worden 1 maal getest


#### BasePremiumTest

##### Het doel van de UnitTest
Deze test test of de basispremie goed wordt berekend

##### Welke data is er gebruikt
1. power = 180 KW
2. prijs = 30000
3. leeftijd = 5 jaar

##### Welke technieken zijn gebruikt om deze data te kiezen
1, 2, 3 dit zijn gewoon willekeurige waardes,
	je krijgt geen extra toeslag ofzo van specifieke waardes
	en het maakt dus niks uit wat je hier invult zolang het maar een normale waarde is

Er is hier gewoon gebruikt gemaakt van de gekozen standaard voor vehicles


## Gevonden fouten

### Program.cs fouten

- rond line 98 stond dat je 1 moest invoeren om maandelijkse betaling te selecteren, 
  terwijl je in de realiteit alles behalve 2 kunt invullen om tot hetzelfde resultaat te komen.
  Ik heb de text aangepast naar "Anything else - Month" om dit goed te weergeven.

	- deze fout heb ik ontdekt door van te voren de applicatie door te lezen om te kijken wat er allemaal gebeurt zodat ik kon weten hoe ik het moest testen
	  uit de gebruikte switch bleek dat elke mogelijke input (behalve '2') werkte als je maand als uitkomst wilde krijgen in plaats van de aangegeven '1'. 
	  Ik heb dus de weergave text aangepast zodat hij bij de code past. 

### Premiumcalculation fouten

- Rond line 52 in de UpdatePRemiumForNoClaimYears methode werd gedeeld door 100 om op het juiste percentage te komen.
  delen door is niet zo accuraat als vermenigvuldigen met (voor procenten) en als je delen door gebruikt dan wordt het antwoord afgerond.
  het afgeronde antwoord werd altijd 0 waardoor de totale waarde van *premium* altijd vermenigvuldigd werd met 0 en dus op 0 uitkwam.
  
  ik heb in plaats van 

	```C#
	(premium * ((100 - NoClaimPercentage) / 100))
	```

	er

	```C#
	(premium * ((100 - NoClaimPercentage) * 0.01))
	```

	van gemaakt om tot een juist resultaat te komen

	- deze fout heb ik ontdekt door van te voren eens door de applicatie heen te lopen en te kijken wat er voor resultaten uit kwamen.
	  Ik heb toen (bijna) overal breakpoints neergezet om mee te lopen met de code om te kijken wat er gebeurt.
	  Na deze berekening stond de premie op 0 in plaats van dat er een percentage vanaf gehaald werd zoals de code het laat lijken.


- Rond line 64 in de PremiumPaymentAmount methode werd **PremiumAmountPerYear / 1.025** gedaan. Dit gaf een te hoog resultaat als antwoord en
  dit heb ik dus vervangen door **PremiumAmountPerYear * 0.975**. Dit levert accuratere antwoorden op en is dus beter
	- deze fout heb ik ontdekt door van te voren de applicatie door te lezen om te kijken wat er allemaal gebeurt zodat ik kon weten hoe ik het moest testen.
	  Ik weet uit ervaring dat gedeeld door berekeningen op deze manier een hoger antwoord terug geven dan dat ze horen te doen en heb hem dus aangepast.

- rond line 70 in de CalculateBasePremium methode gaat de code ervanuit dat er een int berekend wordt en wordt er dus automatisch afgerond. 
  dit is opgelost door er **(double)** voor te zetten waarna het niet langer afgerond werd
	- deze fout heb ik ontdekt door een test run te doen over alleen de CalculateBasePremium methode.
	  er kwam hier een redelijk afgerond antwoord uit terwijl ik dat uit mijn handmatige berekening niet kreeg.