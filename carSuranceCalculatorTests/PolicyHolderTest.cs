﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using Xunit.Sdk;

namespace carSuranceCalculatorTests
{
    public class PolicyHolderTests
    {
        [Theory]
        [InlineData (-1, 0,0)]
        [InlineData(1, 0, 0)]
        [InlineData(10, 4, 20)]
        [InlineData(0, 11, 27)]
        [InlineData(-10, 4, 20)]
        public void LicenseAgeTest(int YearsOld, int MonthsOld, int DaysOld)
        {
            var birthDay = DateTime.Today;
            birthDay = birthDay.AddYears(-YearsOld);
            birthDay = birthDay.AddMonths(-MonthsOld);
            birthDay = birthDay.AddDays(-DaysOld);

            string licenseDate = birthDay.Day.ToString() + "-" + birthDay.Month.ToString() + "-" + birthDay.Year.ToString();
            //licenseDate = "5-10-2005";

            var x = new PolicyHolder(26, licenseDate, 3333, 6);

            Assert.Equal(YearsOld, x.LicenseAge);

        }
    }
}
