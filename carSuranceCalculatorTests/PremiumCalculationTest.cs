﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace carSuranceCalculatorTests
{
    public class PremiumCalculationTests
    {
        [Theory]
        [InlineData(110.33, 28, 5555, 5, 5, 0, 0)] // 5 jaar 0 maanden en 0 dagen als randwaarde voor licenseAge
        [InlineData(126.88, 28, 5555, 5, 4, 11, 27)] // 4 jaar 11 maanden en 27 dagen als randwaarde voor licenseAge (min aantal dagen in een maand is 28) in de realiteit zitten hier een paar dagen speling in aangezien niet elke maand een gelijk aantal dagen heeft en kan het ook werken met een hoger aantal dagen
        [InlineData(110.33, 28, 5555, 5, 6, 10, 20)] // een veilige waarde boven de 5 jaar als extra test
        public void PremiumLicenseAgeSurchargeTest(double expectation, int age, int postalCode, int noClaimYears,
            int licenseYears, int licenseMonths, int licenseDays)
        {
            var holder = setPolicyHolder(age, postalCode, noClaimYears, licenseYears, licenseMonths, licenseDays);

            var premium = new PremiumCalculation(setVehicle(), holder, InsuranceCoverage.WA);

            Assert.Equal(expectation, Math.Round(premium.PremiumAmountPerYear, 2));
        }

        [Theory]
        [InlineData(126.88, 22)] 
        [InlineData(110.33, 23)] 
        [InlineData(110.33, 24)] 
        public void PremiumAgeSurchargeTest(double expectation, int age)
        {
            var holder = setPolicyHolder(age);

            var premium = new PremiumCalculation(setVehicle(), holder, InsuranceCoverage.WA);

            Assert.Equal(expectation, Math.Round(premium.PremiumAmountPerYear, 2));
        }

        [Theory]
        [InlineData(110.33, 28, 5555, 5)]
        [InlineData(104.82, 28, 5555, 6)]
        [InlineData(38.62, 28, 5555, 19)]
        [InlineData(71.72, 28, 5555, 12)]
        public void PremiumNoClaimYearsDiscountTest(double expectation, int age, int postalCode, int noClaimYears)
        {
            var holder = setPolicyHolder(age, postalCode, noClaimYears);

            var premium = new PremiumCalculation(setVehicle(), holder, InsuranceCoverage.WA);

            Assert.Equal(expectation, Math.Round(premium.PremiumAmountPerYear, 2));
        }

        [Theory]
        [InlineData(110.33, 28, 0999)]
        [InlineData(115.85, 28, 1000)]
        [InlineData(115.85, 28, 3599)]
        [InlineData(112.54, 28, 3600)]
        [InlineData(112.54, 28, 4499)]
        [InlineData(110.33, 28, 4500)]
        [InlineData(110.33, 28, 5555)]
        public void PremiumPostalCodeSurchargeTest(double expectation, int age, int postalCode)
        {
            var holder = setPolicyHolder(age, postalCode);

            var premium = new PremiumCalculation(setVehicle(), holder, InsuranceCoverage.WA);

            Assert.Equal(expectation, Math.Round(premium.PremiumAmountPerYear, 2));
        }

        [Theory]
        [InlineData(110.33, "WA")]
        [InlineData(132.4, "WA_PLUS")]
        [InlineData(220.67, "ALL_RISK")]
        public void PremiumInsuranceTypeSurchargeTest(double expectation, string insType)
        {
            InsuranceCoverage insuranceType;

            switch (insType)
            {
                case "WA":
                    insuranceType = InsuranceCoverage.WA;
                    break;
                case "WA_PLUS":
                    insuranceType = InsuranceCoverage.WA_PLUS;
                    break;
                case "ALL_RISK":
                    insuranceType = InsuranceCoverage.ALL_RISK;
                    break;
                default:
                    insuranceType = InsuranceCoverage.WA;
                    break;
            }

            var premium = new PremiumCalculation(setVehicle(), setPolicyHolder(), insuranceType);

            Assert.Equal(expectation, Math.Round(premium.PremiumAmountPerYear, 2));
        }

        [Theory]
        [InlineData(9.19, "MONTH")]
        [InlineData(107.57, "YEAR")]
        public void PremiumMonthlyPaymentSurchargeTest(double expectation, string paymentPeriod = "MONTH")
        {
            var premium = new Mock<PremiumCalculation>(setVehicle(), setPolicyHolder(), InsuranceCoverage.WA);

            PremiumCalculation.PaymentPeriod period;

            if (paymentPeriod == "YEAR")
            {
                period = PremiumCalculation.PaymentPeriod.YEAR;
            }
            else
            {
                period = PremiumCalculation.PaymentPeriod.MONTH;
            }

            Assert.Equal(expectation, premium.Object.PremiumPaymentAmount(period));
        }

        [Fact]
         public void BasePremiumTest()
        {
            var x = setVehicle();
            double y = 110.33;
            Assert.Equal(y, Math.Round(PremiumCalculation.CalculateBasePremium(x), 2));
        }

        private PolicyHolder setPolicyHolder(int age = 28, int postalCode = 5555, int noClaimYears = 5, int licenseYears = 6, int licenseMonths = 10, int licenseDays = 20)
        {
            var birthDay = DateTime.Today;
            birthDay = birthDay.AddYears(-licenseYears);
            birthDay = birthDay.AddMonths(-licenseMonths);
            birthDay = birthDay.AddDays(-licenseDays);

            var licenseDate = birthDay.Day.ToString() + "-" + birthDay.Month.ToString() + "-" + birthDay.Year.ToString();

            return new (age, licenseDate, postalCode, noClaimYears);
        }

        private Vehicle setVehicle(int power = 180, int price = 30000, int vehicleAge = 5)
        {
            return new (power, price, DateTime.Now.Year - vehicleAge);
        }
    }
}
