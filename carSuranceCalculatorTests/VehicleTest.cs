﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace carSuranceCalculatorTests
{
    public class VehicleTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(-1)]
        [InlineData(-10)]
        [InlineData(10)]
        [InlineData(60)]
        public void AgeTest(int YearsOld)
        {
            var constructionYear = DateTime.Now.Year - YearsOld;

            var x = new Vehicle(60, 50000, constructionYear);

            if (YearsOld <= 0)
            {
                Assert.Equal(0, x.Age);
            }
            else
            {
                Assert.Equal(YearsOld, x.Age);
            }
        }
    }
}
